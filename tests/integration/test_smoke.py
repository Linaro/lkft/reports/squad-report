from pathlib import Path
import os
import pytest
import shlex
import subprocess
import testfixtures

testdata = Path.cwd().joinpath("tests", "integration", "testdata")
perf_hook = os.environ.get("PERF_HOOK", "/")


def run(args):
    args = f"squad-report {args}"

    result = subprocess.run(
        shlex.split(args),
        check=True,
        capture_output=True,
        text=True,
    )

    return result


@pytest.mark.parametrize(
    "args, text",
    [
        (
            "--group=lkft --project=linux-next-master --build=next-20240624 --base-build=next-20240621 --template=build --suites=build",  # noqa: E501
            "linux-next-build.txt",
        ),
        (
            "--group=lkft --project=linux-next-master --build=next-20240624 --base-build=next-20240621 --template=report --suites=ltp-smoke,ltp-smoketest --environments=e850-96,rk3399-rock-pi-4b,qemu-arm64",  # noqa: E501
            "linux-next-compare.txt",
        ),
    ],
)
def test_help(args, text):
    with open(testdata.joinpath(text)) as fp:
        expected = fp.read()

    got = run(args)

    testfixtures.compare(got.stdout, expected)
